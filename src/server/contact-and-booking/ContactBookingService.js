//var nodemailer = require('nodemailer');
var Q = require('q');

module.exports = function(msgPayload) {
  var self = this;

  self.name = null;
  self.location = null;
  self.phone = null;
  self.type = null;
  self.additional = null;
  self.date = null;

  self.init = function(_msgPayload) {
    self.name = _msgPayload.name;
    self.location = _msgPayload.location;
    self.phone = _msgPayload.phone;
    self.type = _msgPayload.type;
    self.additional = _msgPayload.additional;
    self.date = _msgPayload.date;
  };

  //TODO: these validation functions have too much copy/paste
  //code. Make a private shared validator method instead

  self.validateName = function() {
    if (!self.name || self.name.trim() === '') return {
      hasError: true,
      msg: 'Enter name'
    };
    return {
      hasError: false,
      msg: null
    };
  };

  self.validateLocation = function() {
    if (!self.location || self.location.trim() === '') return {
      hasError: true,
      msg: 'Enter location'
    };
    return {
      hasError: false,
      msg: null
    };
  };

  //TODO: use regex to validate phone format
  self.validatePhone = function() {
    if (!self.phone || self.phone.trim() === '') return {
      hasError: true,
      msg: 'Enter phone'
    };
    return {
      hasError: false,
      msg: null
    };
  };

  self.validateType = function() {
    if (!self.type || self.type.trim() === '') return {
      hasError: true,
      msg: 'Enter event type'
    };
    return {
      hasError: false,
      msg: null
    };
  };

  self.validateAdditional = function() {
    if (!self.additional || self.additional.trim() === '') return {
      hasError: true,
      msg: 'Enter additional event info'
    };
    return {
      hasError: false,
      msg: null
    };
  };

  //TODO: use moment to validate date
  self.validateDate = function() {
    if (!self.date || self.date.trim() === '') return {
      hasError: true,
      msg: 'Enter event date'
    };
    return {
      hasError: false,
      msg: null
    };
  };

  self.validateObject = function() {
    var nameValidation = self.validateName();
    if (nameValidation.hasError) return nameValidation;

    var locationValidation = self.validateLocation();
    if (locationValidation.hasError) return locationValidation;

    var phoneValidation = self.validatePhone();
    if (phoneValidation.hasError) return phoneValidation;

    var dateValidation = self.validateDate();
    if (dateValidation.hasError) return dateValidation;

    var typeValidation = self.validateType();
    if (typeValidation.hasError) return typeValidation;

    var additionalValidation = self.validateAdditional();
    if (additionalValidation.hasError) return additionalValidation;

    return {
      hasError: false,
      msg: null
    };
  };

  self.sendMessage = function() {
    var dfd = Q.defer();

    //TODO: get service and auth details from configuration
    var apiKey = 'key-266daaa4d8dbdd3e95b48b49419dd9cd';
    var domain = 'sandboxe96d187c50814b77975684ccf9690624.mailgun.org';
    var mailgun = require('mailgun-js')({
      apiKey: apiKey,
      domain: domain
    });

    var msgTemplate = '';
    msgTemplate += 'Name: \t' + self.name + '\n';
    msgTemplate += 'Location: \t' + self.location + '\n';
    msgTemplate += 'Phone: \t' + self.phone + '\n';
    msgTemplate += 'Event Type: \t' + self.type + '\n';
    msgTemplate += 'Event Date: \t' + self.date + '\n';
    msgTemplate += 'Additional Info: \t' + self.additional;

    var data = {
      from: 'Craig Davis <no-reply@craigdavismusic.net>',
      to: 'craigdavismusicnet@gmail.com',
      subject: 'Contact Message From www.CraigDavisMusic.net',
      text: msgTemplate
    };

    mailgun.messages().send(data, function(error, body) {
      if (error) return dfd.reject(error);

      return dfd.resolve();
    });

    return dfd.promise;
  };

  self.init(msgPayload);
};
