var ContactBookingService = require('./ContactBookingService');

module.exports = {
  sendMessage: function(req, res) {
    var c = new ContactBookingService(req.body || {});

    var responseObject = c.validateObject();

    if (responseObject.hasError) return res.status(500).send(responseObject.msg);

    c.sendMessage()
      .then(function() {
        res.status(200).send();
      })
      .catch(function(e) {
        console.log(e);
        return res.status(500).send('Error: could not send email');
      });
  }
};