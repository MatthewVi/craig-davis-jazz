var simpleJadeParser = require('simple-jade-parser');
var express = require('express');
var bodyParser = require('body-parser');
var app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

////////////////////
//CLIENT ENDPOINTS//
////////////////////
app.use(express.static('../../dist'));

app.get('/', function(req, res) {
  return templateHelper({
    file: '../client/home/HomeView.jade',
    data: {
      title: 'Home'
    }
  }, res);
});

app.get('/contact-and-booking', function(req, res) {
  return templateHelper({
    file: '../client/contact-and-booking/ContactBookingView.jade',
    data: {
      title: 'Contact & Booking'
    }
  }, res);
});

app.get('/music-sampler', function(req, res) {
  return templateHelper({
    file: '../client/music-sampler/MusicSamplerView.jade',
    data: {
      title: 'Music Sampler'
    }
  }, res);
});

app.get('/photos', function(req, res) {
  return templateHelper({
    file: '../client/photos/PhotosView.jade',
    data: {
      title: 'Photos'
    }
  }, res);
});

app.get('/calendar', function(req, res) {
  return templateHelper({
    file: '../client/calendar/CalendarView.jade',
    data: {
      title: 'Calendar'
    }
  }, res);
});

//Redirect to home if non-whitelisted route
app.get('*', function(req, res, next) {
  res.redirect('/');
});

/////////////////
//API ENDPOINTS//
/////////////////
var ContactBookingController = require('./contact-and-booking/ContactBookingController');
app.post('/api/contact', ContactBookingController.sendMessage);

///////////
//HELPERS//
///////////
var templateHelper = function(templateData, res) {
  templateData.opts = {
    pretty: true
  };
  simpleJadeParser(templateData)
    .then(function(html) {
      res.status(200).send(html);
    })
    .catch(function(err) {
      res.status(500).send();
      console.error(err);
    });
};

app.listen(8080);
