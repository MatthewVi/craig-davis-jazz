var ContactBookingService = require('./ContactBookingService');
var jQuery = require('jquery');
var _ = require('underscore');
var datepickr = require('../shared/date/datepickr/datepickr');

module.exports = function() {
  var self = this;

  self.name = null;
  self.location = null;
  self.phone = null;
  self.type = null;
  self.additional = null;
  self.date = null;
  self.flashMessageElem = jQuery('.contact-and-booking > form > fieldset > div > .flash');
  self.isValidForm = false;
  self.isSubmitted = false;

  self.construct = function() {
    self.registerFormDatePicker();
    self.registerFormHandler();
  };

  self.submitForm = function() {
    if (!self.isValidForm) return;

    new ContactBookingService().sendMessage(
        self.name,
        self.location,
        self.phone,
        self.type,
        self.additional,
        self.date
      ).done(function() {
        self.isSubmitted = true;
        self.navigate();
      })
      .fail(function(e) {
        if (e && e.responseText !== null) {
          alert(e.responseText);
        } else {
          alert('an error occured, message cannot be sent.');
        }
        
        self.isSubmitted = false;
      });
  };

  self.navigate = function() {
    if (!self.isSubmitted) return;

    alert('Thank you! Craig will be in touch!');
    window.location = '/';
  };

  self.validateForm = function() {
    var _flashMsgBuilder = [];
    if (!self.name || self.name.trim() === '') _flashMsgBuilder.push('- Enter name<br/>');
    if (!self.location || self.location.trim() === '') _flashMsgBuilder.push('- Enter location<br/>');
    if (!self.phone || self.phone.trim() === '') _flashMsgBuilder.push('- Enter phone<br/>');
    if (!self.type || self.type.trim() === '') _flashMsgBuilder.push('- Enter event type<br/>');
    if (!self.additional || self.additional.trim() === '') _flashMsgBuilder.push('- Enter additional event info<br/>');
    if (!self.date || self.date.trim() === '') _flashMsgBuilder.push('- Enter event date<br/>');

    if (_flashMsgBuilder.length !== 0) {
      self.flashMessageElem.html('<p style="color:black">Correct the following:<br/>' + _flashMsgBuilder.join('') + '</p>');
      self.flashMessageElem.removeClass('hide');
    } else {
      self.isValidForm = true;
      self.flashMessageElem.addClass('hide');
    }
  };

  self.normalizeFormData = function(formContext) {
    var _data = _.object(_.map(jQuery(formContext).serializeArray() || [], function(formItem) {
      return [formItem.name, formItem.value];
    }));

    self.name = _data.name;
    self.location = _data.location;
    self.phone = _data.phone;
    self.type = _data.type;
    self.additional = _data.additional;
    self.date = _data.date;
  };

  self.registerFormDatePicker = function() {
    datepickr('#datepicker');
  };

  self.registerFormHandler = function() {
    jQuery('.contact-and-booking .craig-contact').on('submit', function(event) {
      event.preventDefault();

      var formContext = this;

      self.normalizeFormData(formContext);
      self.validateForm();
      self.submitForm();
    });
  };

};