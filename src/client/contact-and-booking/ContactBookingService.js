var jQuery = require('jquery');

module.exports = function() {
  var self = this;

  self.sendMessage = function(name, location, phone, type, additional, date) {
    return jQuery.post('/api/contact', {
      name: name,
      location: location,
      phone: phone,
      type: type,
      additional: additional,
      date: date
    });
  };
};