var jQuery = require('jquery');

module.exports = function() {
  var self = this;

  var standardVideoWidth = '545';
  var standardVideoHeight = '315';

  var videos = [{
    id:          'video_0',
    videoId:     'TbHlTeOCxug',
    height:      standardVideoHeight,
    width:       standardVideoWidth,
    start:       null,
    end:         null,
    instance:    null
  }, {
    id:          'video_1',
    videoId:     'WmjEXSJExrs',
    height:      standardVideoHeight,
    width:       standardVideoWidth,
    start:       '0',
    end:         '226',
    instance:    null
  }, {
    id:          'video_2',
    videoId:     'PSRQod8AW0M',
    height:      standardVideoHeight,
    width:       standardVideoWidth,
    start:       null,
    end:         null,
    instance:    null
  }, {
    id:          'video_3',
    videoId:     'rRr_s2fl9aI',
    height:      standardVideoHeight,
    width:       standardVideoWidth,
    start:       null,
    end:         null,
    instance:    null
  }, {
    id:          'video_4',
    videoId:     '7SiLOxH5XL4',
    height:      standardVideoHeight,
    width:       standardVideoWidth,
    start:       '295',
    end:         '537',
    instance:    null
  }, {
    id:          'video_5',
    videoId:     '4XFfy5jFVI4',
    height:      standardVideoHeight,
    width:       standardVideoWidth,
    start:       null,
    end:         null,
    instance:    null
  }, {
    id:          'video_6',
    videoId:     'Aq3dOj5Oo1A',
    height:      standardVideoHeight,
    width:       standardVideoWidth,
    start:       null,
    end:         null,
    instance:    null
  }, {
    id:          'video_7',
    videoId:     'QT4prP8-zjg',
    height:      standardVideoHeight,
    width:       standardVideoWidth,
    start:       '182',
    end:         '300',
    instance:    null
  }, {
    id:          'video_8',
    videoId:     'KYe6nmxBZuc',
    height:      standardVideoHeight,
    width:       standardVideoWidth,
    start:       '192',
    end:         '274',
    instance:    null
  }, {
    id:          'video_9',
    videoId:     '10Q3gX0TxFI',
    height:      standardVideoHeight,
    width:       standardVideoWidth,
    start:       '471',
    end:         '630',
    instance:    null
  }];

  var updateVideoThatIsPlaying = function(id) {
    pauseAllVideosExcept(id);
  }

  var pauseAllVideosExcept = function(id) {
    videos.forEach(function(v) {
      if (v !== null && v.id !== null && v.instance !== null && v.id !== id) {
        v.instance.pauseVideo();
      }
    })
  }

  self.bindVideosToCards = function() {
    videos.forEach(function(v) {
      var playerVars = {};

      if (v.start != null && v.end != null) {
        playerVars = {
          start: v.start,
          end:   v.end
        };
      }

      v.instance = new YT.Player(v.id, {
        height:  v.height,
        width:   v.width,
        videoId: v.videoId,
        playerVars: playerVars,
        events: {
          'onReady': function() {
          },
          'onStateChange': function(newState) {
            if (newState != null && newState.data != null && newState.data === 1) {
              if (v != null && v.id != null) {
                updateVideoThatIsPlaying(v.id);
              }
            }
          }
        }
      });
    });
  };

  self.construct = function() {
    window.onload = self.bindVideosToCards;
  };
};
