module.exports = {
  HOME: {
    title: 'Home',
    route: ''
  },
  MUSIC_SAMPLER: {
    title: 'Music Sampler',
    route: 'music-sampler'
  },
  CONTACT_AND_BOOKING: {
    title: 'Contact & Booking',
    route: 'contact-and-booking'
  },
  PHOTOS: {
    title: 'Photos',
    route: 'photos'
  },
  CALENDAR: {
    title: 'Calendar',
    route: 'calendar'
  }
};
