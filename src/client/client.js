var Pages = require('./shared/navigation/Pages');
var jQuery = require('jquery');

jQuery(document).ready(function() {
  var cheapBrowserCheck = function() {
    if (!window.btoa) {
      window.location = 'http://www.old-browser.org/en-us/?referer=www.craigdavisjazz.com';
    }
  }();

  //very simple "routing" mechanism
  var currentRoute = window.location.pathname.replace('/', '');

  switch (currentRoute.replace('/', '')) {
    case Pages.HOME.route:
      var HomeController = require('./home/HomeController');
      new HomeController().construct();
      break;
    case Pages.CONTACT_AND_BOOKING.route:
      var ContactBookingController = require('./contact-and-booking/ContactBookingController');
      new ContactBookingController().construct();
      break;
    case Pages.MUSIC_SAMPLER.route:
      var MusicSamplerController = require('./music-sampler/MusicSamplerController');
      new MusicSamplerController().construct();
      break;
    case Pages.PHOTOS.route:
      var PhotosController = require('./photos/PhotosController');
      new PhotosController().construct();
      break;
    case Pages.CALENDAR.route:
      var CalendarController = require('./calendar/CalendarController');
      new CalendarController().construct();
      break;
  }
});
