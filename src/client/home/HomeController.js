var jQuery = require('jquery');
var howler = require('howler');

module.exports = function() {
  var self = this;

  var audioPlayer = null;

  var registerAudioPlayerHandler = function() {
    audioPlayer = new howler.Howl({
      urls: ['/home/sampler.mp3', '/home/sampler.ogg'],
      autoplay: true,
      loop: false,
      volume: 1.0,
      buffer: true
    });
  };

  var isMobileBrowser = function() {
    if (navigator.userAgent.match(/Android/i) ||
      navigator.userAgent.match(/webOS/i) ||
      navigator.userAgent.match(/iPhone/i) ||
      navigator.userAgent.match(/iPad/i) ||
      navigator.userAgent.match(/iPod/i) ||
      navigator.userAgent.match(/BlackBerry/i) ||
      navigator.userAgent.match(/Windows Phone/i)) {
      return true;
    } else {
      return false;
    }
  };

  var registerAudioButtonHandler = function() {
    var btn = jQuery('.home .audio-button');

    var states = {
      PAUSE: '♫ Pause',
      PLAY: '♫ Resume'
    };

    if(isMobileBrowser()) {
      btn.text(states.PLAY);
      btn.hide();

      setTimeout(function() {
         btn.show();
      }, 2500);
    } else {
      btn.text(states.PAUSE);
    }

    btn.on('click', function() {
      var currentState = btn.text();

      switch (currentState) {
        case states.PLAY:
          btn.text(states.PAUSE);
          audioPlayer.play();
          break;
        case states.PAUSE:
          btn.text(states.PLAY);
          audioPlayer.pause();
          break;
      }

    });
  };

  self.construct = function() {
    registerAudioPlayerHandler();
    registerAudioButtonHandler();
  };
};
