# Craig Davis Jazz

##### Note -> make this repo public when you do work... it is now private

Setup
-----

```bash
$ sudo apt-get update
$ sudo apt-get install -y build-essential python-software-properties curl
$ curl -sL https://deb.nodesource.com/setup_10.x | sudo bash -
$ sudo apt-get install -y nodejs
$ sudo npm install -g gulp pm2
$ sudo apt-get install -y nginx
$ cd ~/
$ git clone https://MatthewVi@bitbucket.org/MatthewVi/craig-davis-jazz.git
$ cd ~/craig-davis-jazz
$ sudo npm install
package.json => nano add "natives": "^1.1.6"
npm install natives@1.1.6
$ gulp production
$ cd ./src/server 
$ # I spent WAY too much time getting relative links to work. You gotta run from this dir
$ pm2 start server.js --name="website"
$ pm2 save
$ pm2 startup
$ pm2 list
$ sudo nano /etc/nginx/sites-available/default (REMOVE OTHER STUFF)
server {
    listen 80;

    location / {
        proxy_pass http://localhost:8080;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
    }
}
$ sudo service nginx restart
check 157.230.9.69 
```

Update
------

```bash
$ ssh root@104.131.87.252
$ rm -rf website.tar craig-davis-jazz-production
$ pm2 delete all
$ sudo service nginx stop
$ exit
$ rm -rf website.tar
$ tar -cvf website.tar craig-davis-jazz-production
$ chmod 777 website.tar
$ scp website.tar root@104.131.87.252:/root/
$ ssh root@104.131.87.252
$ tar -xvf website.tar
$ cd craig-davis-jazz-production
$ #run necessary install steps
$ reboot
```