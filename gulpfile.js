var gulp = require('gulp');
var browserify = require('gulp-browserify');
var uglify = require('gulp-uglify');
var cssmin = require('gulp-cssmin');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
var gulpif = require('gulp-if');
var yargs = require('yargs').argv;

var buildLocation = './dist/';
var srcLocation = './src/client/';
var npmLocation = './node_modules/';

var tasks = {
  JAVASCRIPT: 'JAVASCRIPT',
  CSS: 'CSS',
  HTML: 'HTML',
  IMAGES: 'IMAGES',
  MUSIC: 'MUSIC',
  WATCH: 'WATCH',
  SITEMAP: 'SITEMAP',
  DEFAULT: 'DEFAULT'.toLowerCase(),      //gulp tasks are case sensitive...
  PRODUCTION: 'PRODUCTION'.toLowerCase() //gulp tasks are case sensitive...
};

gulp.task(tasks.JAVASCRIPT, function() {
  //necessary to ensure this variable is boolean and non-nullable,
  //this allows for uglify to be issued (or not). Is useful for debugging
  //<example>
  //   $ gulp --uglify=false
  //</example>
  var uglifyFlag = yargs.uglify === false ? false : true;

  gulp.src(srcLocation + 'client.js')
    .pipe(browserify({
      insertGlobals: true,
      debug: true
    }))
    .pipe(gulpif(uglifyFlag, uglify()))
    .pipe(rename('index.min.js'))
    .pipe(gulp.dest(buildLocation));
});

gulp.task(tasks.CSS, function() {
  gulp.src([
      srcLocation + '**/*.css'
    ])
    .pipe(concat('index.min.css'))
    .pipe(cssmin())
    .pipe(gulp.dest(buildLocation));
});

gulp.task(tasks.HTML, function() {
  gulp.src(srcLocation + '**/*.html')
    .pipe(gulp.dest(buildLocation));
});

gulp.task(tasks.IMAGES, function() {
  gulp.src(srcLocation + '**/*.{png,ico,jpg,svg}')
    .pipe(gulp.dest(buildLocation));
});

gulp.task(tasks.MUSIC, function() {
  gulp.src(srcLocation + '**/*.{mp3,ogg}')
    .pipe(gulp.dest(buildLocation));
});

gulp.task(tasks.SITEMAP, function() {
  gulp.src(srcLocation + 'sitemap.xml')
    .pipe(gulp.dest(buildLocation));
});

gulp.task(tasks.WATCH, function() {
  gulp.watch(srcLocation + '**/*.css', [tasks.CSS]);
  gulp.watch(srcLocation + '**/*.js', [tasks.JAVASCRIPT]);
  gulp.watch(srcLocation + '**/*.html', [tasks.HTML]);
  gulp.watch(srcLocation + '**/*.{png,ico,jpg,svg}', [tasks.IMAGES]);
});

gulp.task(tasks.DEFAULT, [
  tasks.JAVASCRIPT,
  tasks.CSS,
  tasks.HTML,
  tasks.IMAGES,
  tasks.MUSIC,
  tasks.SITEMAP,
  tasks.WATCH
]);

gulp.task(tasks.PRODUCTION, [
  tasks.JAVASCRIPT,
  tasks.CSS,
  tasks.HTML,
  tasks.IMAGES,
  tasks.MUSIC,
  tasks.SITEMAP,
]);
